Application.
============

The system is built using the PHP Codeigniter framework which is an MVC framework.

Database.
=========
The database design is MySQL 

Functionality.
==============

* Admin side with content management. The admins are able to log in and view dashboard analytics, manage site information.
* Add/remove/update products.
* Add product to cart.
* Empty cart.
* Choose payment method.
* Pay using VISA/ Mastercard/ AMEX/ MTN Mobile Money/ Airtel Money/ MPESA.
* Add a blog.
* select style of site.
* select language of site.
* choose the colour of your site.
* add/remove admins.