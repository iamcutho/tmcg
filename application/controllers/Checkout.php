<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include($_SERVER['DOCUMENT_ROOT'] . '/tmcg/application/classes/OAuth.php');
class Checkout extends MY_Controller
{

    private $orderId;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('currency_convertor'));
        $this->load->library('form_validation');
        $this->load->model('mobilemoneymodel');
    }

    public function index()
    {
        $data = array();
        $head = array();
        $arrSeo = $this->Publicmodel->getSeo('page_checkout');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);

        if (isset($_POST['payment_type'])) {
            $errors = $this->userInfoValidate($_POST);
            if (!empty($errors)) {
                $this->session->set_flashdata('submit_error', $errors);
            } else {
                $_POST['referrer'] = $this->session->userdata('referrer');
                $_POST['clean_referrer'] = cleanReferral($_POST['referrer']);
                $orderId = $this->Publicmodel->setOrder($_POST);
                if ($orderId != false) {
                    $this->orderId = $orderId;
                    $this->goToDestination();
                } else {
                    log_message('error', 'Cant save order!! ' . implode('::', $_POST));
                    $this->session->set_flashdata('order_error', true);
                    redirect(LANG_URL . '/checkout/order-error');
                }
            }
        }

        $data['cashondelivery_visibility'] = $this->AdminModel->getValueStore('cashondelivery_visibility');
        $data['paypal_email'] = $this->AdminModel->getValueStore('paypal_email');
        $data['bestSellers'] = $this->Publicmodel->getbestSellers();
        $this->render('checkout', $head, $data);
    }

    private function goToDestination()
    {
        if ($_POST['payment_type'] == 'cashOnDelivery' || $_POST['payment_type'] == 'Bank') {
            $this->shoppingcart->clearShoppingCart();
            $this->session->set_flashdata('success_order', true);
        }
        if ($_POST['payment_type'] == 'Bank') {
            $_SESSION['order_id'] = $this->orderId;
            redirect(LANG_URL . '/checkout/successbank');
        }
        if ($_POST['payment_type'] == 'cashOnDelivery') {
            redirect(LANG_URL . '/checkout/successcash');
        }
        if ($_POST['payment_type'] == 'PayPal') {
            @set_cookie('paypal', $this->orderId, 2678400);
            redirect(LANG_URL . '/checkout/paypalpayment');
        }
        if ($_POST['payment_type'] == 'mobileMoney') {
            $_SESSION['order_id'] = $this->orderId;
            redirect(LANG_URL . '/checkout/pesapalpayment');
        }
    }

    private function userInfoValidate($post)
    {
        $errors = array();
        if (mb_strlen(trim($post['first_name'])) == 0) {
            $errors[] = lang('first_name_empty');
        }
        if (mb_strlen(trim($post['last_name'])) == 0) {
            $errors[] = lang('last_name_empty');
        }
        if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
            $errors[] = lang('invalid_email');
        }
        $post['phone'] = preg_replace("/[^0-9]/", '', $post['phone']);
        if (mb_strlen(trim($post['phone'])) == 0) {
            $errors[] = lang('invalid_phone');
        }
        if (mb_strlen(trim($post['address'])) == 0) {
            $errors[] = lang('address_empty');
        }
        if (mb_strlen(trim($post['city'])) == 0) {
            $errors[] = lang('invalid_city');
        }
        return $errors;
    }

    public function orderError()
    {
        if ($this->session->flashdata('order_error')) {
            $data = array();
            $head = array();
            $arrSeo = $this->Publicmodel->getSeo('page_checkout');
            $head['title'] = @$arrSeo['title'];
            $head['description'] = @$arrSeo['description'];
            $head['keywords'] = str_replace(" ", ",", $head['title']);
            $this->render('checkout_parts/order_error', $head, $data);
        } else {
            redirect(LANG_URL . '/checkout');
        }
    }

    public function mobileMoneyPayment($msg = NULL)
    {
        $data = array();
        $head = array();
        $arrSeo = $this->Publicmodel->getSeo('page_checkout');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $data['msg'] = $msg;
        $this->render('checkout_parts/mobilemoney_payment', $head, $data);
    }

    public function pesapalPayment($msg = NULL)
    {
        $data = array();
        $head = array();
        $arrSeo = $this->Publicmodel->getSeo('page_checkout');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $data['msg'] = $msg;
        $this->render('checkout_parts/pesapal', $head, $data);
    }

    public function mobileMoneyProcess()
    {
        $this->form_validation->set_rules('mobile_money_number', 'Mobile Money Number', 'trim|required');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
        $data = array();
        $head = array();
        $arrSeo = $this->Publicmodel->getSeo('page_checkout');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        if ($this->form_validation->run() == false) {
            $this->mobileMoneyPayment(validation_errors());
            return;
        } else {
            $mobile_money_number = $this->input->post('mobile_money_number');
            $amount = $this->input->post('amount');
            $payment = $this->mobilemoneymodel->depositPull($amount, $mobile_money_number, "ecommerce payment for TMCG", "payment complete", time());
            $xml = simplexml_load_string($payment);

            $json = json_encode($xml);
            $array = json_decode($json, TRUE);
            $status = $array['Response']['Status'];
            if (isset($status) && $status == "OK") {
                $data['conf_num'] = $array['Response']['TransactionReference'];
                $data['amount'] = $amount;
                $this->render('checkout_parts/mobilemoney_success', $head, $data);
            } else {
                $data = array();
                $head = array();
                $head['title'] = "Mobile money error";
                $head['description'] = "There was an error using mobile money to checkout";
                $head['keywords'] = str_replace(" ", ",", $head['title']);
                $this->render('checkout_parts/mobile_money_error', $head, $data);
            }
        }
    }

    public function paypalPayment()
    {
        $data = array();
        $head = array();
        $arrSeo = $this->Publicmodel->getSeo('page_checkout');
        $head['title'] = @$arrSeo['title'];
        $head['description'] = @$arrSeo['description'];
        $head['keywords'] = str_replace(" ", ",", $head['title']);
        $data['paypal_sandbox'] = $this->AdminModel->getValueStore('paypal_sandbox');
        $data['paypal_email'] = $this->AdminModel->getValueStore('paypal_email');
        $data['paypal_currency'] = $this->AdminModel->getValueStore('paypal_currency');
        $this->render('checkout_parts/paypal_payment', $head, $data);
    }

    public function successPaymentCashOnD()
    {
        if ($this->session->flashdata('success_order')) {
            $data = array();
            $head = array();
            $arrSeo = $this->Publicmodel->getSeo('page_checkout');
            $head['title'] = @$arrSeo['title'];
            $head['description'] = @$arrSeo['description'];
            $head['keywords'] = str_replace(" ", ",", $head['title']);
            $this->render('checkout_parts/payment_success_cash', $head, $data);
        } else {
            redirect(LANG_URL . '/checkout');
        }
    }

    public function successPaymentBank()
    {
        if ($this->session->flashdata('success_order')) {
            $data = array();
            $head = array();
            $arrSeo = $this->Publicmodel->getSeo('page_checkout');
            $head['title'] = @$arrSeo['title'];
            $head['description'] = @$arrSeo['description'];
            $head['keywords'] = str_replace(" ", ",", $head['title']);
            $data['bank_account'] = $this->AdminModel->getBankAccountSettings();
            $this->render('checkout_parts/payment_success_bank', $head, $data);
        } else {
            redirect(LANG_URL . '/checkout');
        }
    }

    public function paypal_cancel()
    {
        if (get_cookie('paypal') == null) {
            redirect(base_url());
        }
        @delete_cookie('paypal');
        $orderId = get_cookie('paypal');
        $this->Publicmodel->changePaypalOrderStatus($orderId, 'canceled');
        $data = array();
        $head = array();
        $head['title'] = '';
        $head['description'] = '';
        $head['keywords'] = '';
        $this->render('checkout_parts/paypal_cancel', $head, $data);
    }

    public function paypal_success()
    {
        if (get_cookie('paypal') == null) {
            redirect(base_url());
        }
        @delete_cookie('paypal');
        $this->shoppingcart->clearShoppingCart();
        $orderId = get_cookie('paypal');
        $this->Publicmodel->changePaypalOrderStatus($orderId, 'payed');
        $data = array();
        $head = array();
        $head['title'] = '';
        $head['description'] = '';
        $head['keywords'] = '';
        $this->render('checkout_parts/paypal_success', $head, $data);
    }

    function pesapal_iframe()
    {
        $this->form_validation->set_rules('amount', 'Amount', 'required|trim');
        $this->form_validation->set_rules('description', 'Description', 'required|trim');
        $this->form_validation->set_rules('type', 'Type', 'required|trim');
        $this->form_validation->set_rules('reference', 'Reference', 'required|trim');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->index();
            return;
        } else {

            //pesapal params
            $token = $params = NULL;
            if (!is_numeric($this->input->post('amount'))) {
                $msg = "<p style='color: red'>Please use a valid amount.</p>";
                $this->index($msg);
                return;
            }
            $amount = $this->input->post('amount');
            $amount = number_format($amount, 2);
            $desc = $this->input->post('description');
            $type = $this->input->post('type');
            $reference = $this->input->post('reference');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $phonenumber = "";
            $consumer_key = '3gjx8CYQuDci4HgxLGED87YytdAOpQaW';
            $consumer_secret = 'uBptyx+AVm1tQPL2mV+z4BN8I0Q=';
            $signature_method = new OAuthSignatureMethod_HMAC_SHA1();
            $iframelink = 'https://www.pesapal.com/API/PostPesapalDirectOrderV4';//production

            $callback_url = base_url() . '/tmcg/checkout_parts/pesapal_process'; //redirect url, the page that will handle the response from pesapal.

            $post_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <PesapalDirectOrderInfo
                xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
                xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" Amount=\"" . $amount . "\"
                Description=\"" . $desc . "\" Type=\"" . $type . "\"
                Reference=\"" . $reference . "\" FirstName=\"" . $first_name . "\"
                LastName=\"" . $last_name . "\"
                Email=\"" . $email . "\"
                PhoneNumber=\"" . $phonenumber . "\" xmlns=\"http://www.pesapal.com\" />";
            $post_xml = htmlentities($post_xml);

            $consumer = new OAuthConsumer($consumer_key, $consumer_secret);
            //post transaction to pesapal
            $iframe_src = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $iframelink, $params);

            $iframe_src->set_parameter("oauth_callback", $callback_url);
            $iframe_src->set_parameter("pesapal_request_data", $post_xml);
            $iframe_src->sign_request($signature_method, $consumer, $token);
            $data['iframe_src'] = $iframe_src;
            $data['iframe'] = "<iframe src=" . $iframe_src . " width='100%' height='700px'  scrolling='no' frameBorder='0'>
	<p>Browser unable to load iFrame</p>
</iframe>";
            $data1 = array(
                'Username' => $email,
                'Merc_Ref' => $reference,
                'Track_Id' => "",
                'Pesa_Amount' => $amount,
                'Status' => 'START'
            );

            $this->mobilemoneymodel->pesapal_save_status($data1);
            $head = array();
            $head['title'] = '';
            $head['description'] = '';
            $head['keywords'] = '';
            $this->shoppingcart->clearShoppingCart();
            $this->render('checkout_parts/pesapal_iframe', $head, $data);
        }
    }

    function pesapal_track_purchase()
    {
        $this->form_validation->set_rules('pesapal_merchant_reference', 'Pesapal Merchant Reference', 'required|trim');
        $this->form_validation->set_rules('pesapal_tracking_id', 'Pesapal Tracking Id', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->index();
            return;
        } else {
            $merch_ref = $this->input->post('pesapal_merchant_reference');
            $track_id = $this->input->post('pesapal_tracking_id');
            $this->mobilemoneymodel->pesapal_update($merch_ref, $track_id, "CHANGE");
            $amountArray = $this->mobilemoneymodel->check_pesapal_amount($merch_ref);
            foreach ($amountArray as $key => $value) {
                $amount = $value->pesa_amount;
            }

            $consumer_key = "3gjx8CYQuDci4HgxLGED87YytdAOpQaW";
            $consumer_secret = "uBptyx+AVm1tQPL2mV+z4BN8I0Q=";
            $statusrequestAPI = 'https://www.pesapal.com/api/querypaymentstatus';
            $pesapalNotification = "CHANGE";
            $pesapalTrackingId = $track_id;
            $pesapal_merchant_reference = $merch_ref;
            $signature_method = new OAuthSignatureMethod_HMAC_SHA1();
            if ($pesapalNotification == "CHANGE" && $pesapalTrackingId != '') {
                $token = $params = NULL;
                $consumer = new OAuthConsumer($consumer_key, $consumer_secret);
                $request_status = OAuthRequest::from_consumer_and_token($consumer,
                    $token, "GET", $statusrequestAPI, $params);
                $request_status->set_parameter("pesapal_merchant_reference", $pesapal_merchant_reference);
                $request_status->set_parameter("pesapal_transaction_tracking_id", $pesapalTrackingId);
                $request_status->sign_request($signature_method, $consumer, $token);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $request_status);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                if (defined('CURL_PROXY_REQUIRED')) if (CURL_PROXY_REQUIRED == 'True') {
                    $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') &&
                        strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
                    curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
                    curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
                    curl_setopt
                    ($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
                }
                $response = curl_exec($ch);
                $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
                $raw_header = substr($response, 0, $header_size - 4);
                $headerArray = explode("\r\n\r\n", $raw_header);
                $header = $headerArray[count($headerArray) - 1];

                //transaction status
                $elements = preg_split("/=/", substr($response, $header_size));
                $status = $elements[1];
                $pesapalNotification = $status;
                curl_close($ch);

                $update = $this->mobilemoneymodel->pesapal_status_update($merch_ref, $status);
//UPDATE YOUR DB TABLE WITH NEW STATUS FOR TRANSACTION WITH pesapal_transaction_tracking_id $pesapalTrackingId
                if (isset($status)) {
                    if ($status == 'FAILED') {
                        $data = array();
                        $head = array();
                        $head['title'] = '';
                        $head['description'] = '';
                        $head['keywords'] = '';
                        $this->render('checkout_parts/pesapal_error', $head, $data);
                        return;
                    } else if ($status == 'PENDING') {
                        $data['message'] = "Your payment is pending, visit the pending payments menu item to keep track of this payment.";
                        $head = array();
                        $head['title'] = '';
                        $head['description'] = '';
                        $head['keywords'] = '';
                        $this->render('checkout_parts/pesapal_process', $head, $data);
                    } else if ($status == 'COMPLETED') {
                        $data = array();
                        $head = array();
                        $head['title'] = '';
                        $head['description'] = '';
                        $head['keywords'] = '';
                        $this->render('checkout_parts/pesapal_success', $head, $data);
                    } else if ($status == 'INVALID') {
                        $data['message'] = "Your payment was not successful, because your transaction was considered invalid.";
                        $head = array();
                        $head['title'] = '';
                        $head['description'] = '';
                        $head['keywords'] = '';
                        $this->render('checkout_parts/pesapal_error', $head, $data);
                    }
                }
            }
        }

    }
}
