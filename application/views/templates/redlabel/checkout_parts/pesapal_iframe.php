<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="container">
        <div class="body">
<?php
if (isset($iframe)){
    echo $iframe;
}else{
    echo "There was a problem with this transaction, please try again!";
}
?>
        </div>
    </div>