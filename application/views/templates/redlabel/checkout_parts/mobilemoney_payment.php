<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    <?php
    if (!empty($cartItems['array'])) {
        ?>
        <div align="center" class="container row">
            <div class="col-md-6">
                <img src="<?= base_url('assets/imgs/mtnmoney.jpg') ?>"/>
            </div>
            <div class="col-md-6">
                <img src="<?= base_url('assets/imgs/airtel-money.jpg') ?>"/>
            </div>
        </div>
        <div class="alert alert-info text-center"><?= lang('you_chose_mobilemoney') ?></div>
        <hr>
        <form action="<?php echo base_url('checkout/mobilemoneyprocess') ?>" method="post" target="_top" role="form">
            <div class="alert-dismissable alert-danger"><?php echo isset($msg) ? $msg : "" ?></div>
            <?php
            $i = 1;
            $total = 0;
            foreach ($cartItems['array'] as $item) {
                ?>
                <input type="hidden" name="item_name_<?= $i ?>" value="<?php echo $item['title'] ?>">
                <input type="hidden" name="amount_<?= $i ?>" value="<?php echo $item['price'] ?>">
                <input type="hidden" name="quantity_<?= $i ?>" value="<?php echo $item['num_added'] ?>">
                <?php
                $total += $item['price'];
                $i++;
            }
            ?>
            <input type="hidden" name="amount" value="<?php echo $total ?>">
            <div class="col-md-6">
                <input class="form-control" type="text" name="mobile_money_number" placeholder="Enter mobile money number (begin with 256)" />
            </div>
            <input type="hidden" value="utf-8" name="charset">
            <input type="hidden" value="<?= base_url('checkout/mobilemoney_success') ?>" name="return">
            <input type="hidden" value="<?= base_url('checkout/mobilemoney_cancel') ?>" name="cancel_return">
            <input type="hidden" value="authorization" name="paymentaction">
            <a href="<?= base_url('checkout') ?>" class="btn btn-lg btn-danger btm-10"><?= lang('cancel_payment') ?></a>
            <button type="submit" class="btn btn-lg btn-success btm-10"><?= lang('go_to_mobile_money') ?></button>
        </form>
    <?php
    } else {
        redirect(base_url());
    }
    ?>
</div>