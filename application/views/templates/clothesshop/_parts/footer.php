<footer>
    <div class="container">
        <span class="footer-text"><?php echo $footerCopyright ?></span>
    </div>
</footer>
<?php if ($this->session->flashdata('emailAdded')) { ?>
<script>
    $(document).ready(function () {
        ShowNotificator('alert-info', '<?php echo lang('email_added') ?>');
    });
</script>
<?php
}
echo $addedJs;
?>
</div>
</div>
<div id="notificator" class="alert"></div>
<script src="<?php echo base_url('templatejs/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/placeholders.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-datepicker.min.js') ?>"></script>
<script>
var variable = {
    clearShoppingCartUrl: "<?php echo base_url('clearShoppingCart') ?>",
    manageShoppingCartUrl: "<?php echo base_url('manageShoppingCart') ?>"
};
</script>
<script src="<?php echo base_url('assets/js/system.js') ?>"></script>
<script src="<?php echo base_url('templatejs/mine.js') ?>"></script>
</body>
</html>
