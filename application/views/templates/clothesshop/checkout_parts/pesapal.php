<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    <div class="body">
    <?php
    if (isset($message)){
        echo $message;
    }else{
        echo "";
    }
    ?>
    <?php
    if (isset($error_msg)){
        echo $error_msg;
    }else{
        echo "";
    }
    ?>
    <h1>Pay with<br>
        <small>MOBILE MONEY, VISA, MASTERCARD ETC.</small>
    </h1>
    <?php echo validation_errors(); ?>
    <form action="pesapal_iframe" method="post" role="form">
        <div id="amount" class="form-group">
            <label for="id_amount">Amount:</label>
            <input type="text" size="20" name="amount" id="id_amount" class="form-control" required />
        </div>
        <!-- <div class="form-group">
            <label for="amount">Type:</label> -->
        <input type="hidden" size="20" name="type" id="type" value="MERCHANT" class="form-control"/>
        <!--
        </div> -->
        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" size="20" placeholder="Enter description" name="description" id="description" class="form-control" required />
        </div>
        <div class="form-group">
            <label for="reference">Reference:</label>
            <input type="text" size="20" value="<?php print mt_rand(); ?>" name="reference" id="reference" class="form-control" readonly />
        </div>
        <div class="form-group">
            <label for="first_name">First Name:</label>
            <input type="text" size="20" placeholder="Enter your first name" name="first_name" id="description" class="form-control" required />
        </div>
        <div class="form-group">
            <label for="last_name">Last Name:</label>
            <input type="text" size="20" placeholder="Enter your last name" name="last_name" id="description" class="form-control" required />
        </div>
        <div class="form-group">
            <label for="email">Email Address:</label>
            <input type="text" size="20" placeholder="Enter your email address" name="email" id="email" class="form-control" required />
        </div>
        <input class="btn btn-info" type="submit" value="Make Payment" />
    </form>
    </div>
</div>