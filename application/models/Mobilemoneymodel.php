<?php
/**
 * Created by PhpStorm.
 * User: cuthbert
 * Date: 3/12/2017
 * Time: 11:48 PM
 */

class Mobilemoneymodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        ini_set('max_execution_time', 300);
        define('API_USERNAME', '90000084098 '); // set api username
        define('API_PASS', '1140878316'); // set api password
        define('API_URL', 'https://41.220.12.206/services/yopaymentsdev/task.php'); // set api url
        $this->xml_data = '<?xml version="1.0" encoding="UTF-8"?>' .
            '<AutoCreate>' .
            '<Request>' .
            '<APIUsername>' . API_USERNAME . '</APIUsername>' .
            '<APIPassword>' . API_PASS . '</APIPassword>';
        $this->ch = curl_init(API_URL);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml', 'Content-transfer-encoding: text'));
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
    }

    //deposit to mobilemoney account(Pull Method)
    public function depositPull($amount, $account, $narrative, $provref, $extref){
        $this->xml_data .='<Method>acdepositfunds</Method>'.
            '<Amount>'.$amount.'</Amount>'.
            '<Account>'.$account.'</Account>'.
            '<Narrative>'.$narrative.'</Narrative> '.
            '<ProviderReferenceText>' . $provref . '</ProviderReferenceText>'.
            '<ExternalReference>' . $extref . '</ExternalReference>'.
            '</Request>'.
            '</AutoCreate>';

        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->xml_data);
        return curl_exec($this->ch);
    }

    public function pesapal_save_status($data){
        $this->db->insert('pesapal', $data);
    }
    public function pesapal_track_status($merc_ref, $track_id){
        $this->db->select('*');
        $this->db->from('pesapal');
        $this->db->where('merc_ref', $merc_ref);
        $this->db->select('track_id', $track_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0){
            return $query->num_rows();
        }
    }

    public function pesapal_status_update($merc_ref, $status){
        $data2 = array(
            'status' => $status
        );
        $this->db->where('merc_ref', $merc_ref);
        $this->db->update('pesapal', $data2);
    }

    public function pesapal_update($merc_ref,$track_id,$status){
        $data = array(
            'merc_ref' => $merc_ref,
            'track_id' => $track_id,
            'status' => $status
        );
        $this->db->where('merc_ref', $merc_ref);
        $this->db->update('pesapal', $data);
    }

    public function check_pesapal_amount($merc_ref){
        $this->db->select('pesa_amount');
        $this->db->from('pesapal');
        $this->db->where('merc_ref', $merc_ref);
        $query = $this->db->get();

        if ($query->num_rows() == 1){
            return $query->result();
        }
    }

    public function check_pesapal_pending($username,$status){
        $this->db->select('*');
        $this->db->from('pesapal');
        $this->db->where('username', $username);
        $this->db->where('status', $status);

        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
    }

}