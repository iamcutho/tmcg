-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 29, 2018 at 06:08 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tmcg`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_pages`
--

DROP TABLE IF EXISTS `active_pages`;
CREATE TABLE IF NOT EXISTS `active_pages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `active_pages`
--

INSERT INTO `active_pages` (`id`, `name`, `enabled`) VALUES
(1, 'blog', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bank_accounts`
--

DROP TABLE IF EXISTS `bank_accounts`;
CREATE TABLE IF NOT EXISTS `bank_accounts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `iban` varchar(255) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `bic` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank_accounts`
--

INSERT INTO `bank_accounts` (`id`, `name`, `iban`, `bank`, `bic`) VALUES
(1, 'Amayomart Ltd', 'BG2342423424e', 'Commercial Bank of Africa', 'F3ADA');

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

DROP TABLE IF EXISTS `blog_posts`;
CREATE TABLE IF NOT EXISTS `blog_posts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `time` int(10) UNSIGNED NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `image`, `url`, `time`) VALUES
(1, 'thumb_129533.jpg', 'The-future-is-here_1', 1491132762);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cookie_law`
--

DROP TABLE IF EXISTS `cookie_law`;
CREATE TABLE IF NOT EXISTS `cookie_law` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `theme` varchar(20) NOT NULL,
  `visibility` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cookie_law`
--

INSERT INTO `cookie_law` (`id`, `link`, `theme`, `visibility`) VALUES
(1, '#', 'dark-bottom', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cookie_law_translations`
--

DROP TABLE IF EXISTS `cookie_law_translations`;
CREATE TABLE IF NOT EXISTS `cookie_law_translations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `message` varchar(255) NOT NULL,
  `button_text` varchar(50) NOT NULL,
  `learn_more` varchar(50) NOT NULL,
  `abbr` varchar(5) NOT NULL,
  `for_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`abbr`,`for_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cookie_law_translations`
--

INSERT INTO `cookie_law_translations` (`id`, `message`, `button_text`, `learn_more`, `abbr`, `for_id`) VALUES
(2, 'This site uses cookies and by continuing to use it you agree to the terms and conditions', 'ok', 'help', 'en', 1);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity` varchar(255) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `time` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `abbr` varchar(5) NOT NULL,
  `name` varchar(30) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `currencyKey` varchar(5) NOT NULL,
  `flag` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `abbr`, `name`, `currency`, `currencyKey`, `flag`) VALUES
(2, 'en', 'english', 'UGSH', 'UGX', 'en.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `mobile_money`
--

DROP TABLE IF EXISTS `mobile_money`;
CREATE TABLE IF NOT EXISTS `mobile_money` (
  `mm_id` int(11) NOT NULL AUTO_INCREMENT,
  `mm_username` varchar(50) NOT NULL,
  `mm_password` int(255) NOT NULL,
  PRIMARY KEY (`mm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile_money`
--

INSERT INTO `mobile_money` (`mm_id`, `mm_username`, `mm_password`) VALUES
(1, '4353535', 35345345);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `products` text NOT NULL,
  `date` int(10) UNSIGNED NOT NULL,
  `referrer` varchar(255) NOT NULL,
  `clean_referrer` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `paypal_status` varchar(10) DEFAULT NULL,
  `processed` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'viewed status is change when change processed status',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `products`, `date`, `referrer`, `clean_referrer`, `payment_type`, `paypal_status`, `processed`, `viewed`) VALUES
(1, 1234, 'a:1:{i:2;s:1:\"2\";}', 1489260546, 'http://localhost/', 'localhost', 'cashOnDelivery', NULL, 0, 0),
(2, 1235, 'a:1:{i:4;s:1:\"1\";}', 1489349015, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(3, 1236, 'a:1:{i:4;s:1:\"1\";}', 1489349229, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(4, 1237, 'a:1:{i:4;s:1:\"1\";}', 1489528667, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(5, 1238, 'a:1:{i:4;s:1:\"1\";}', 1489529202, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(6, 1239, 'a:1:{i:4;s:1:\"1\";}', 1489529633, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(7, 1240, 'a:1:{i:4;s:1:\"1\";}', 1489529688, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(8, 1241, 'a:1:{i:4;s:1:\"1\";}', 1489529940, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(9, 1242, 'a:1:{i:4;s:1:\"1\";}', 1489530604, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(10, 1243, 'a:1:{i:4;s:1:\"1\";}', 1489531009, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(11, 1244, 'a:1:{i:4;s:1:\"1\";}', 1489531177, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(12, 1245, 'a:1:{i:4;s:1:\"1\";}', 1489531495, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(13, 1246, 'a:1:{i:4;s:1:\"1\";}', 1489609986, 'http://localhost/', 'localhost', 'mobileMoney', NULL, 0, 0),
(14, 1247, 'a:1:{i:2;s:1:\"1\";}', 1492089235, 'http://localhost/', 'localhost', 'mobileMoney', NULL, 0, 0),
(15, 1248, 'a:1:{i:2;s:1:\"1\";}', 1492953290, 'http://localhost/', 'localhost', 'mobileMoney', NULL, 0, 0),
(16, 1249, 'a:1:{i:2;s:1:\"1\";}', 1512312145, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(17, 1250, 'a:1:{i:4;s:1:\"1\";}', 1512312358, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(18, 1251, 'a:1:{i:16;s:1:\"1\";}', 1512313428, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(19, 1252, 'a:1:{i:2;s:1:\"1\";}', 1512316083, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(20, 1253, 'a:1:{i:2;s:1:\"1\";}', 1512316311, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(21, 1254, 'a:1:{i:2;s:1:\"1\";}', 1512372479, 'Direct', 'Direct', 'cashOnDelivery', NULL, 0, 0),
(22, 1255, 'a:1:{i:2;s:1:\"1\";}', 1512372509, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0),
(23, 1256, 'a:1:{i:4;s:1:\"1\";}', 1512458294, 'Direct', 'Direct', 'cashOnDelivery', NULL, 0, 0),
(24, 1257, 'a:1:{i:10;s:1:\"1\";}', 1535565995, 'Direct', 'Direct', 'cashOnDelivery', NULL, 0, 0),
(25, 1258, 'a:1:{i:10;s:1:\"1\";}', 1535566025, 'Direct', 'Direct', 'mobileMoney', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders_clients`
--

DROP TABLE IF EXISTS `orders_clients`;
CREATE TABLE IF NOT EXISTS `orders_clients` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(20) NOT NULL,
  `post_code` varchar(10) NOT NULL,
  `notes` text NOT NULL,
  `for_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders_clients`
--

INSERT INTO `orders_clients` (`id`, `first_name`, `last_name`, `email`, `phone`, `address`, `city`, `post_code`, `notes`, `for_id`) VALUES
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'plot 5 kay street', 'kampala', '256', 'please get me black if you can', 1),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'laddad', 'kampala', '256', 'alkskdasd', 2),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'akjdsaksjdka', 'kampala', '256', 'jasdjahsd', 3),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', '123, kintu stretch', 'kampala', '256', 'yo yo yo', 4),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'jajskd adakldja', 'kampala', '256', 'jksf sjfksdfs kfsdf', 5),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'hjfhsd fsjdhf', 'kampala', '256', 'jskdf sdkdjfs', 6),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'kjkjsd fkjsjfjks ', 'kampala', '256', 'jkskf sdkjfs', 7),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'jkfjksf sdfdkjsdjfsd', 'kampala', '256', 'kjsdkjfsd', 8),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'jkfs fkjsdjf', 'kampala', '256', 'hjfhsdf sdhfdsd fskjdfs', 9),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'jkfskjdf sfkjskjdfs', 'kampala', '256', 'kjfskfjs fsjdfksf ksfjks', 10),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'jfskjf sjkfs', 'kampala', '256', 'nfskdf skjfsdf', 11),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'kjfjsf sfjjkskfs skdfks', 'kampala', '256', 'kjskf sjfjksf', 12),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'jkhh hjkj', 'kampala', '256', 'hjgjh hjhkjhkj', 13),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'hhjghhj hj', 'kampala', '256', 'bfhhj hjhj', 14),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'jolkljhjh', 'kampala', '256', 'fedfg htyut tguy', 15),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'dasd', 'kla', '00256', 'dasd', 16),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'plot 1038, bukoto-kisaasi road', 'kampala', '00256', 'adasd', 17),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '774523181', 'plot 1038, bukoto-kisaasi road', 'kampala', '00256', 'dasdasd', 18),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '774523181', 'plot 1038, bukoto-kisaasi road', 'kampala', '00256', 'dasdasd', 19),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '774523181', 'plot 1038, bukoto-kisaasi road', 'kampala', '00256', 'adasd', 20),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '774523181', 'kasubi view, house 173\r\nmakerere university', 'Kampala', '90003', 'adasdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 21),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '774523181', 'kasubi view, house 173\r\nmakerere university', 'Kampala', '00256', 'dasadasd', 22),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '774523181', 'kasubi view, house 173\r\nmakerere university', 'Kampala', '', 'dasdas', 23),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'kasubi view, house 173\r\nmakerere university', 'Kampala', '00256', 'dss dsad', 24),
(0, 'Cuthbert', 'Asingwire', 'iamcutho@gmail.com', '0774523181', 'kasubi view, house 173\r\nmakerere university', 'Kampala', '00256', 'dewew rwr', 25);

-- --------------------------------------------------------

--
-- Table structure for table `pesapal`
--

DROP TABLE IF EXISTS `pesapal`;
CREATE TABLE IF NOT EXISTS `pesapal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `merc_ref` varchar(255) NOT NULL,
  `track_id` varchar(255) NOT NULL,
  `pesa_amount` int(9) NOT NULL,
  `status` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `merc_ref` (`merc_ref`),
  UNIQUE KEY `merc_ref_2` (`merc_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesapal`
--

INSERT INTO `pesapal` (`id`, `username`, `merc_ref`, `track_id`, `pesa_amount`, `status`) VALUES
(25, 'iamcutho@gmail.com', '1579245446', '', 500, 'START'),
(26, 'iamcutho@gmail.com', '24221029', '', 500, 'START'),
(27, 'iamcutho@gmail.com', '1616357746', '', 500, 'START'),
(28, 'iamcutho@gmail.com', '324472789', '', 500, 'START'),
(29, 'iamcutho@gmail.com', '332013116', '', 500, 'START'),
(30, 'iamcutho@gmail.com', '898368328', '', 500, 'START'),
(31, 'iamcutho@gmail.com', '1195651416', '', 500, 'START'),
(32, 'iamcutho@gmail.com', '393282486', '', 500, 'START'),
(33, 'iamcutho@gmail.com', '1183908224', '', 500, 'START'),
(34, 'iamcutho@gmail.com', '616868401', '', 500, 'START'),
(35, 'iamcutho@gmail.com', '402469817', '', 500, 'START'),
(36, 'iamcutho@gmail.com', '1235336209', '', 500, 'START'),
(37, 'iamcutho@gmail.com', '1073502276', '', 1, 'START');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `folder` int(10) UNSIGNED DEFAULT NULL COMMENT 'folder with images',
  `image` varchar(255) NOT NULL,
  `time` int(10) UNSIGNED NOT NULL COMMENT 'time created',
  `time_update` int(10) UNSIGNED NOT NULL COMMENT 'time updated',
  `visibility` tinyint(1) NOT NULL DEFAULT '1',
  `shop_categorie` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `procurement` int(10) UNSIGNED NOT NULL,
  `in_slider` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `brand_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `folder`, `image`, `time`, `time_update`, `visibility`, `shop_categorie`, `quantity`, `procurement`, `in_slider`, `url`, `brand_id`) VALUES
(2, 1487532899, 'sandals1.jpg', 1487533004, 1489270578, 1, 7, 2, 0, 0, 'womens_sandals_2', 0),
(3, 1487533158, 'mens_sandals.png', 1487533238, 0, 1, 4, 4, 0, 0, 'mens_sandals_3', 0),
(4, 1487533482, 'necklace.JPG', 1487533536, 0, 1, 5, 5, 0, 0, 'necklace_4', 0),
(7, 1493241444, 'DSC05211.jpg', 1493241572, 0, 1, 4, 2, 0, 0, 'Wooden_Mask_6', 0),
(10, 1493588364, 'IMG-20170301-WA0019.jpg', 1493588477, 1493588682, 1, 8, 1, 0, 1, 'Orange_Gladiator_Sandals_9', 0),
(15, 1497256373, 'rsz_picture39.jpg', 1497256671, 1499463835, 1, 43, 0, 0, 0, 'Kitchen_gloves_15', 0),
(16, 1497256691, 'rsz_dsc05278(2).jpg', 1497256825, 1499463788, 1, 46, 1, 0, 0, 'African_Woman_Paiting_16', 0),
(17, 1497256844, 'DSC05221.jpg', 1497256986, 0, 1, 44, 1, 0, 0, 'African_Mask_17', 0),
(18, 1497257660, 'rsz_picture14.jpg', 1497257853, 1499775571, 1, 45, 1, 0, 0, 'Woven_basket_18', 0),
(19, 1497257988, 'rsz_picture7.jpg', 1497258102, 1499464144, 1, 44, 1, 0, 0, 'African_Map_Carving_19', 0),
(20, 1499088141, 'img1.JPG', 1499088322, 0, 1, 14, 0, 0, 0, 'Elephant_Curving_20', 0),
(21, 1499326309, '82.JPG', 1499326427, 1499329126, 1, 33, 1, 0, 0, 'Bead_Cuff_21', 0),
(22, 1499326438, '9.JPG', 1499326523, 1499329179, 1, 33, 1, 0, 0, 'Beaded_Cuff_22', 0),
(23, 1499326568, '13.JPG', 1499326759, 1499326811, 1, 32, 1, 0, 0, 'Cross_bead_necklace_23', 0),
(24, 1499326853, '15.JPG', 1499326957, 0, 1, 32, 1, 0, 0, 'Fancy_drape_necklace_24', 0),
(25, 1499326980, '17.JPG', 1499327138, 0, 1, 32, 1, 0, 0, 'Flower_Necklace_25', 0),
(26, 1499327146, '19.JPG', 1499327277, 0, 1, 32, 1, 0, 0, 'Bead_embroidered_Necklace_26', 0),
(27, 1499327283, '20.JPG', 1499327407, 0, 1, 32, 1, 0, 0, 'Cross_bead_necklace_27', 0),
(28, 1499327467, '21.JPG', 1499327641, 0, 1, 32, 1, 0, 0, 'Chunky_Turquoise_wonder_28', 0),
(29, 1499327667, '22.JPG', 1499327789, 0, 1, 32, 1, 0, 0, 'Peaches_and_Cream_Crescent_29', 0),
(30, 1499327857, '24.JPG', 1499328185, 0, 1, 32, 1, 0, 0, 'White_pearls_and_beads_30', 0),
(31, 1499328212, '26.JPG', 1499328304, 0, 1, 32, 1, 0, 0, 'Cross_bead_necklace_31', 0),
(32, 1499328349, '27.JPG', 1499328458, 0, 1, 32, 1, 0, 0, 'Shaggy_green_Necklace_32', 0),
(33, 1499773416, 'rsz_1picture2.jpg', 1499773591, 1499773682, 1, 44, 1, 0, 0, 'African_Woman_curving_33', 0),
(34, 1499773693, 'rsz_1picture38.jpg', 1499773848, 0, 1, 43, 1, 0, 1, 'Stuffed_Rhino_34', 0),
(35, 1499773864, 'rsz_1picture61.jpg', 1499773983, 0, 1, 44, 1, 0, 1, 'Men_in_Boat_35', 0),
(36, 1499773989, 'rsz_dsc05281.jpg', 1499774093, 0, 1, 46, 1, 0, 1, 'Gorilla_Painting_36', 0),
(37, 1499775268, 'rsz_dsc05282.jpg', 1499775349, 0, 1, 46, 1, 0, 1, 'Men_on_boat_painting_37', 0),
(38, 1499775393, 'rsz_dsc05283.jpg', 1499775453, 0, 1, 46, 1, 0, 1, 'Giraffe_Painting_38', 0);

-- --------------------------------------------------------

--
-- Table structure for table `seo_pages`
--

DROP TABLE IF EXISTS `seo_pages`;
CREATE TABLE IF NOT EXISTS `seo_pages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seo_pages`
--

INSERT INTO `seo_pages` (`id`, `name`) VALUES
(1, 'home'),
(2, 'checkout'),
(3, 'contacts'),
(4, 'blog');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
CREATE TABLE IF NOT EXISTS `shopping_cart` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_id` varchar(100) NOT NULL,
  `article_id` int(11) NOT NULL,
  `time` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shop_categories`
--

DROP TABLE IF EXISTS `shop_categories`;
CREATE TABLE IF NOT EXISTS `shop_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sub_for` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_categories`
--

INSERT INTO `shop_categories` (`id`, `sub_for`) VALUES
(30, 0),
(15, 14),
(14, 0),
(52, 29),
(51, 29),
(29, 0),
(53, 29),
(40, 30),
(32, 30),
(33, 30),
(34, 30),
(35, 0),
(36, 35),
(41, 30),
(38, 35),
(39, 35),
(42, 30),
(43, 0),
(44, 43),
(45, 43),
(46, 43),
(47, 43),
(48, 43),
(49, 43),
(50, 43),
(54, 29);

-- --------------------------------------------------------

--
-- Table structure for table `subscribed`
--

DROP TABLE IF EXISTS `subscribed`;
CREATE TABLE IF NOT EXISTS `subscribed` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscribed`
--

INSERT INTO `subscribed` (`id`, `email`, `browser`, `ip`, `time`) VALUES
(1, 'iamcutho@gmail.com', 'Mozilla/5.0 (Windows NT 6.3; rv:49.0) Gecko/20100101 Firefox/49.0', '::1', '1489270298');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `basic_description` text NOT NULL,
  `price` varchar(20) NOT NULL,
  `old_price` varchar(20) NOT NULL,
  `abbr` varchar(5) NOT NULL,
  `for_id` int(11) NOT NULL COMMENT ' ',
  `type` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `art_id_abbr` (`abbr`,`for_id`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `title`, `description`, `basic_description`, `price`, `old_price`, `abbr`, `for_id`, `type`, `name`) VALUES
(1, '', '', '', '', '', 'bg', 1, 'page', ''),
(2, '', '', '', '', '', 'en', 1, 'page', ''),
(36, '', '', '', '', '', 'en', 14, 'shop_categorie', 'Entertainment'),
(13, '', '', '', '', '', 'en', 4, 'shop_categorie', 'Clothing'),
(37, '', '', '', '', '', 'en', 15, 'shop_categorie', 'Musical Instruments'),
(9, '', '', '', '', '', 'bg', 2, 'shop_categorie', 'kettle'),
(10, '', '', '', '', '', 'en', 2, 'shop_categorie', 'kettle'),
(11, '', '', '', '', '', 'gr', 2, 'shop_categorie', 'kettle'),
(38, '', '', '', '', '', 'en', 16, 'shop_categorie', 'Clothing'),
(17, 'women\'s sandals', '<p>beautiful multi-coloured sandals</p>\r\n', '', '12000', '15000', 'en', 2, 'product', ''),
(18, 'men\'s sandals', '<p>beautiful pointed men&#39;s sandals for all weather purpose</p>\r\n', '', '10000', '', 'en', 3, 'product', ''),
(19, 'necklace', '<p>beautiful necklace that can go on office and home wear</p>\r\n', '', '15000', '', 'en', 4, 'product', ''),
(21, 'The future is here', '<p>Forget the hassles of moving around town to look for all your favorite African clothing articles.</p>\r\n', '', '', '', 'en', 1, 'blog', ''),
(23, 'Wooden Mask', '<p>Mahogany wooden African mask</p>\r\n', '', '60000', '', 'en', 7, 'product', ''),
(27, '', '', '', '', '', 'en', 11, 'shop_categorie', 'Entertainment'),
(99, 'Men in Boat', '<p>Wooden Carving of men in a boat made with mahogany wood.</p>\r\n', '', '60000', '', 'en', 35, 'product', ''),
(31, 'Orange Gladiator Sandals', '<p>Orange Gladiator Sandals with mixed seed bead detail.</p>\r\n', '', '60000', '', 'en', 10, 'product', ''),
(98, 'Stuffed Rhino', '<p>Stuffed Rhino in african fabric.</p>\r\n', '', '10000', '', 'en', 34, 'product', ''),
(97, 'African Woman carving', '<p>Wooden curving of African woman for house decor</p>\r\n', '', '45000', '', 'en', 33, 'product', ''),
(79, 'Kitchen gloves', '<p>Kitchen cooking gloves made from assorted african fabric.</p>\r\n', '', '15000', '', 'en', 15, 'product', ''),
(45, '', '', '', '', '', 'en', 23, 'shop_categorie', 'Women\'s clothing'),
(46, '', '', '', '', '', 'en', 24, 'shop_categorie', 'Clothing'),
(44, '', '', '', '', '', 'en', 22, 'shop_categorie', 'Men\'s Clothing'),
(73, '', '', '', '', '', 'en', 51, 'shop_categorie', 'Women\'s clothing'),
(74, '', '', '', '', '', 'en', 52, 'shop_categorie', 'Men\'s Clothing'),
(51, '', '', '', '', '', 'en', 29, 'shop_categorie', 'Clothing'),
(52, '', '', '', '', '', 'en', 30, 'shop_categorie', 'Jewellery and Accessories '),
(62, '', '', '', '', '', 'en', 40, 'shop_categorie', 'Earrings'),
(54, '', '', '', '', '', 'en', 32, 'shop_categorie', 'Necklaces'),
(55, '', '', '', '', '', 'en', 33, 'shop_categorie', 'Bracelets'),
(56, '', '', '', '', '', 'en', 34, 'shop_categorie', 'Rings'),
(57, '', '', '', '', '', 'en', 35, 'shop_categorie', 'Shoes'),
(58, '', '', '', '', '', 'en', 36, 'shop_categorie', 'Men\'s Sandals'),
(60, '', '', '', '', '', 'en', 38, 'shop_categorie', 'Women\'s Sandals'),
(61, '', '', '', '', '', 'en', 39, 'shop_categorie', 'Children\'s Sandals'),
(63, '', '', '', '', '', 'en', 41, 'shop_categorie', 'Hairbands'),
(64, '', '', '', '', '', 'en', 42, 'shop_categorie', 'Bags'),
(65, '', '', '', '', '', 'en', 43, 'shop_categorie', 'Home and Living'),
(66, '', '', '', '', '', 'en', 44, 'shop_categorie', 'Wooden Carvings'),
(67, '', '', '', '', '', 'en', 45, 'shop_categorie', 'Baskets'),
(68, '', '', '', '', '', 'en', 46, 'shop_categorie', 'Paintings'),
(69, '', '', '', '', '', 'en', 47, 'shop_categorie', 'Batiks and wall hangings'),
(70, '', '', '', '', '', 'en', 48, 'shop_categorie', 'Photoframes'),
(71, '', '', '', '', '', 'en', 49, 'shop_categorie', 'Lampshades'),
(72, '', '', '', '', '', 'en', 50, 'shop_categorie', 'Table mats'),
(75, '', '', '', '', '', 'en', 53, 'shop_categorie', 'Children\'s clothing'),
(76, '', '', '', '', '', 'en', 54, 'shop_categorie', 'Assorted African Fabrics'),
(80, 'African Woman Painting', '<p>Beautiful painting of African woman with child on back.</p>\r\n', '', '80000', '', 'en', 16, 'product', ''),
(81, 'African Mask', '<p>Mahogany African mask</p>\r\n', '', '65000', '', 'en', 17, 'product', ''),
(82, 'Woven basket', '<p>Black and Orange woven basket. Medium Size.</p>\r\n', '', '20000', '', 'en', 18, 'product', ''),
(83, 'African Map Carving', '<p>Mahogany African map carving with animal detail.</p>\r\n', '', '90000', '', 'en', 19, 'product', ''),
(84, 'Elephant Curving', '<p>Small elephant curving made out of mahogany wood.</p>\r\n', '', '', '', 'en', 20, 'product', ''),
(85, 'Bead Cuff', '<p>Bead embroidery hand cuff with red lotus flower detail.</p>\r\n', '', '15000', '', 'en', 21, 'product', ''),
(86, 'Beaded Cuff', '<p>Bead woven cuff.</p>\r\n', '', '10000', '', 'en', 22, 'product', ''),
(87, 'Cross bead necklace', '<p>Orange and black large cross net beaded necklace.</p>\r\n', '', '25000', '', 'en', 23, 'product', ''),
(88, 'Fancy drape necklace', '<p>Yellow and blue beaded drape necklace with flower detail.</p>\r\n', '', '30000', '', 'en', 24, 'product', ''),
(89, 'Flower Necklace', '<p>Cream and black flower beaded necklace. Made with plastic beads.</p>\r\n', '', '25000', '', 'en', 25, 'product', ''),
(90, 'Bead embroidered Necklace', '<p>Beautiful glass bead embroidered necklace with earrings.</p>\r\n', '', '80000', '', 'en', 26, 'product', ''),
(91, 'Cross bead necklace', '<p>Cross bead necklace with star shape detail in turquoise, black and gold.</p>\r\n', '', '25000', '', 'en', 27, 'product', ''),
(92, 'Chunky Turquoise wonder', '<p>A beautiful beaded necklace with large pearl bead details.</p>\r\n', '', '70000', '', 'en', 28, 'product', ''),
(93, 'Peaches and Cream Crescent', '<p>Cream bead embroidered necklace with peach bead detail and pearl bead shaped like a crescent moon.</p>\r\n', '', '65000', '', 'en', 29, 'product', ''),
(94, 'White pearls and beads', '<p>White beaded pearl necklace with leafy detail good for bridal jewelry.</p>\r\n', '', '30000', '', 'en', 30, 'product', ''),
(95, 'Cross bead necklace', '<p>Cross bead necklace in star shape form with buggle beads.</p>\r\n', '', '25000', '', 'en', 31, 'product', ''),
(96, 'Shaggy green Necklace', '<p>Green and black frilled necklace with earrings.</p>\r\n', '', '30000', '', 'en', 32, 'product', ''),
(100, 'Gorilla Painting', '<p>Water painting of face of a Gorilla.</p>\r\n', '', '50000', '', 'en', 36, 'product', ''),
(101, 'Men on boat painting', '<p>Canvas painting of men on a boat.</p>\r\n', '', '50000', '', 'en', 37, 'product', ''),
(102, 'Giraffe Painting', '<p>Giraffe water painting</p>\r\n', '', '60000', '', 'en', 38, 'product', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(100) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'notifications by email',
  `last_login` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `notify`, `last_login`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'your@email.com', 0, 1535565417);

-- --------------------------------------------------------

--
-- Table structure for table `value_store`
--

DROP TABLE IF EXISTS `value_store`;
CREATE TABLE IF NOT EXISTS `value_store` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `thekey` varchar(50) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `key` (`thekey`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `value_store`
--

INSERT INTO `value_store` (`id`, `thekey`, `value`) VALUES
(1, 'sitelogo', 'tmcg.jpg'),
(2, 'navitext', 'TMCG'),
(3, 'footercopyright', 'Powered fastgo ltd © All right reserved. '),
(4, 'contactspage', '<p>Feel free to contact us anytime during working hours</p>\r\n'),
(5, 'footerContactAddr', 'entebbe road'),
(6, 'footerContactEmail', 'amayomartltd@gmail.com'),
(7, 'footerContactPhone', '+256773139858'),
(8, 'googleMaps', '0.3005696, 32.5572833'),
(9, 'footerAboutUs', 'Your number one African wear online store'),
(10, 'footerSocialFacebook', ''),
(11, 'footerSocialTwitter', ''),
(12, 'footerSocialGooglePlus', ''),
(13, 'footerSocialPinterest', ''),
(14, 'footerSocialYoutube', ''),
(16, 'contactsEmailTo', 'contacts@shop.dev'),
(17, 'shippingOrder', '50000'),
(18, 'addJs', ''),
(19, 'publicQuantity', '0'),
(20, 'paypal_email', ''),
(21, 'paypal_sandbox', '1'),
(22, 'paypal_currency', 'EUR'),
(23, 'publicDateAdded', '0'),
(24, 'googleApi', ''),
(25, 'template', 'redlabel'),
(26, 'cashondelivery_visibility', '1'),
(27, 'showBrands', '0'),
(28, 'showInSlider', '0'),
(29, 'newStyle', '.gradient-color {\r\n    background-color: rgba(211, 84, 0, 0.78);\r\n    /* IE9, iOS 3.2+ */\r\n    background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAxIDEiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxsaW5lYXJHcmFkaWVudCBpZD0idnNnZyIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIHgxPSIwJSIgeTE9IjAlIiB4Mj0iMCUiIHkyPSIxMDAlIj48c3RvcCBzdG9wLWNvbG9yPSIjZDM1NDAwIiBzdG9wLW9wYWNpdHk9IjAuNCIgb2Zmc2V0PSIwIi8+PHN0b3Agc3RvcC1jb2xvcj0iI2QzNTQwMCIgc3RvcC1vcGFjaXR5PSIxIiBvZmZzZXQ9IjAuNzIiLz48c3RvcCBzdG9wLWNvbG9yPSIjZDM1NDAwIiBzdG9wLW9wYWNpdHk9IjEiIG9mZnNldD0iMC44NDQwMDAwMDAwMDAwMDAxIi8+PHN0b3Agc3RvcC1jb2xvcj0iI2QzNTQwMCIgc3RvcC1vcGFjaXR5PSIxIiBvZmZzZXQ9IjEiLz48L2xpbmVhckdyYWRpZW50PjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiIGZpbGw9InVybCgjdnNnZykiIC8+PC9zdmc+);\r\n    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%,color-stop(0, rgba(211, 84, 0, 0.4)),color-stop(0.72, #d35400),color-stop(0.844, #d35400),color-stop(1, #d35400));\r\n    /* Android 2.3 */\r\n    background-image: -webkit-repeating-linear-gradient(top,rgba(211, 84, 0, 0.4) 0%,#d35400 72%,#d35400 84.4%,#d35400 100%);\r\n    /* IE10+ */\r\n    background-image: repeating-linear-gradient(to bottom,rgba(211, 84, 0, 0.4) 0%,#d35400 72%,#d35400 84.4%,#d35400 100%);\r\n    background-image: -ms-repeating-linear-gradient(top,rgba(211, 84, 0, 0.4) 0%,#d35400 72%,#d35400 84.4%,#d35400 100%);\r\nborder : solid 1px rgba(211, 84, 0, 0.78);box-shadow : 2px 2px 3px rgba(211, 84, 0, 0.78); }\r\n.mine-color, .btn-inner-search { background-color:rgba(211, 84, 0, 0.78); }\r\n.button-more::before { border-color: transparent rgba(211, 84, 0, 0.78); }\r\n.btn-go-search:active,.btn-go-search:focus, .btn-go-search:hover, .button-more:hover, .button-more:focus, .button-more:hover { background-color: #c8; }\r\ndiv.filter-sidebar .title, .title.alone {border-bottom: 3px solid #dd; }\r\ndiv.filter-sidebar .title span, .title.alone span {border-bottom : 3px solid rgba(211, 84, 0, 0.78); }\r\n.cloth-bg-color {background-color:rgba(211, 84, 0, 0.78);}\r\n.cloth-color {color:rgba(211, 84, 0, 0.78);}\r\n.cloth--border-color {border-color:rgba(211, 84, 0, 0.78);}\r\n.cloth-border-top-color {border-top-color: rgba(211, 84, 0, 0.78);}\r\n.navbar.cloth .navbar-nav li.active a { color:rgba(211, 84, 0, 0.78);}\r\n.navbar.cloth .navbar-nav li a:hover, .navbar.cloth .navbar-nav li a:focus {background-color:transparent !important; color:rgba(211, 84, 0, 0.78);}\r\n.navbar.cloth .navbar-nav li:hover, .navbar.cloth .navbar-nav li.active {border-top:3px solid; border-top-color: rgba(211, 84, 0, 0.78);}\r\n#small_carousel .product-list div.add-to-cart, #small_carousel .product-list .info-btn {background-color: rgba(211, 84, 0, 0.78); }\r\n.products .product-list div.add-to-cart, .products .product-list .info-btn { background-color: rgba(211, 84, 0, 0.78);}\r\n.part-label { background-color: rgba(211, 84, 0, 0.78);}\r\n.pagination li a { color:rgba(211, 84, 0, 0.78);}\r\n.pagination li.active a {background-color: rgba(211, 84, 0, 0.78); border-color:rgba(211, 84, 0, 0.78); }\r\n.list li a {background-color:rgba(211, 84, 0, 0.78);}\r\n#home-slider .carousel-indicators .active, #small_carousel .carousel-indicators .active {background-color: rgba(211, 84, 0, 0.78);}\r\n.btn-blue-round {    background-color: rgba(211, 84, 0, 0.78);\r\n    /* IE9, iOS 3.2+ */\r\n    background-image: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAxIDEiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxsaW5lYXJHcmFkaWVudCBpZD0idnNnZyIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIHgxPSIwJSIgeTE9IjAlIiB4Mj0iMCUiIHkyPSIxMDAlIj48c3RvcCBzdG9wLWNvbG9yPSIjZDM1NDAwIiBzdG9wLW9wYWNpdHk9IjAuNCIgb2Zmc2V0PSIwIi8+PHN0b3Agc3RvcC1jb2xvcj0iI2QzNTQwMCIgc3RvcC1vcGFjaXR5PSIxIiBvZmZzZXQ9IjAuNzIiLz48c3RvcCBzdG9wLWNvbG9yPSIjZDM1NDAwIiBzdG9wLW9wYWNpdHk9IjEiIG9mZnNldD0iMC44NDQwMDAwMDAwMDAwMDAxIi8+PHN0b3Agc3RvcC1jb2xvcj0iI2QzNTQwMCIgc3RvcC1vcGFjaXR5PSIxIiBvZmZzZXQ9IjEiLz48L2xpbmVhckdyYWRpZW50PjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiIGZpbGw9InVybCgjdnNnZykiIC8+PC9zdmc+);\r\n    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%,color-stop(0, rgba(211, 84, 0, 0.4)),color-stop(0.72, #d35400),color-stop(0.844, #d35400),color-stop(1, #d35400));\r\n    /* Android 2.3 */\r\n    background-image: -webkit-repeating-linear-gradient(top,rgba(211, 84, 0, 0.4) 0%,#d35400 72%,#d35400 84.4%,#d35400 100%);\r\n    /* IE10+ */\r\n    background-image: repeating-linear-gradient(to bottom,rgba(211, 84, 0, 0.4) 0%,#d35400 72%,#d35400 84.4%,#d35400 100%);\r\n    background-image: -ms-repeating-linear-gradient(top,rgba(211, 84, 0, 0.4) 0%,#d35400 72%,#d35400 84.4%,#d35400 100%);\r\n}\r\n.my-basket span.sum-scope {color:rgba(211, 84, 0, 0.78);}\r\n.search .btn-red { border-bottom: 1px solid #dd; border-right: 1px solid #dd ; border-left: 1px solid #dd ;}\r\n#small_carousel .product-list h2 a {color:rgba(211, 84, 0, 0.78);}\r\n#home-slider .item h1 a {color:rgba(211, 84, 0, 0.78);}\r\n\r\n/* IE8- CSS hack */\r\n@media \\0screen\\,screen\\9 {\r\n    .gradient-color {\r\n        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#66d35400\",endColorstr=\"#ffd35400\",GradientType=0);\r\n    }\r\n}');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
